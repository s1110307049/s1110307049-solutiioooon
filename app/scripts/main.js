var Mediator = (function () {

    var channels = {};

    //
    // Subscribe to a channel
    //
    function subscribe(channel, fn, that) {
        if (!channels[channel]) {
            channels[channel] = [];
        }
        channels[channel].push({
            context: that,
            callback: fn
        });
    }

    //
    // Publish a channel (like triggering an event)
    //
    function publish(channel) {

        if (!channels[channel]) {
            return false;
        }
        var args = Array.prototype.slice.call(arguments, 1);
        for (var i = 0, l = channels[channel].length; i < l; i += 1) {
            var subscription = channels[channel][i];
            if (typeof subscription.context === undefined) {
                subscription.callback(args);
            } else {
                subscription.callback.call(subscription.context, args);
            }
        }
    }

    return {
        subscribe: subscribe,
        publish: publish
    }

})();

window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame       ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame    ||
          function( callback ){
            window.setTimeout(callback, 1000 / 60);
          };
})();

var BackendConnector = (function () {
    function send() {
        var xhr = $.ajax({
            url: "http://fettblog.eu/fh/numbers.php?jsonp",
            dataType: 'jsonp',
            success: function (data) {
                Mediator.publish('datareceived', data);
            },
            error: function (err) {

            }
        });
    }
    Mediator.subscribe('on-keypress', send);
})();

///////======================== logic

var lifes = 3;

var points = 0;

var playing = false; 

var first, second, third;

$(window).on('click', function() {
	if(!playing) {
  		Mediator.publish('on-keypress');
  		playing = true;
  	}
});

$(window).on('keypress', function() {
	if(!playing) {
		Mediator.publish('on-keypress');
		playing = true;
	}
});


Mediator.subscribe('on-keypress', function() {
	$('#box1').removeClass('cherry');
	$('#box1').removeClass('enemy');
	$('#box1').removeClass('veggie');
	$('#box1').removeClass('star');
	$('#box2').removeClass('cherry');
	$('#box2').removeClass('enemy');
	$('#box2').removeClass('veggie');
	$('#box2').removeClass('star');
	$('#box3').removeClass('cherry');
	$('#box3').removeClass('enemy');
	$('#box3').removeClass('veggie');
	$('#box3').removeClass('star');
});

Mediator.subscribe('on-keypress', function() {
	$('#lifes').text(--lifes);

	requestAnimFrame(function() {
		$('#box1').addClass('cherryAnimationClass');
	});

	requestAnimFrame(function() {
		$('#box2').addClass('enemyAnimationClass');
	});

	requestAnimFrame(function() {
		$('#box3').addClass('veggieAnimationClass');
	});
});

Mediator.subscribe('datareceived', function() {
  first = arguments[0][0]["slots"][0];
  second = arguments[0][0]["slots"][1];
  third = arguments[0][0]["slots"][2];
  points = arguments[0][0]["result"];
});

Mediator.subscribe('box1end', function() {
  $('#box1').removeClass('cherryAnimationClass');
  if(first == 1)
  	$('#box1').addClass('cherry');
  else if(first == 2)
  	$('#box1').addClass('enemy');
  else if(first == 3)
  	$('#box1').addClass('veggie');
  else 
  	$('#box1').addClass('star');
});

Mediator.subscribe('box2end', function() {
  $('#box2').removeClass('enemyAnimationClass');
  if(second == 1)
  	$('#box2').addClass('cherry');
  else if(second == 2)
  	$('#box2').addClass('enemy');
  else if(second == 3)
  	$('#box2').addClass('veggie');
  else 
  	$('#box2').addClass('star');
});

Mediator.subscribe('box3end',  function() {
  $('#box3').removeClass('veggieAnimationClass');
  if(third == 1)
  	$('#box3').addClass('cherry');
  else if(third == 2)
  	$('#box3').addClass('enemy');
  else if(third == 3)
  	$('#box3').addClass('veggie');
  else 
  	$('#box3').addClass('star');

  $('#coins').text(points);
  lifes += points;
  $('#lifes').text(lifes);

  playing = false;
  if(lifes == 0) {
 	alert("You looooooooooooooooooset very hardest...");
 	alert("hardly possible...");
 	location.reload();
  }
});

$('#box1').bind('animationend webkitAnimationEnd MSAnimationEnd oAnimationEnd', function(){
	Mediator.publish('box1end');
});

$('#box2').bind('animationend webkitAnimationEnd MSAnimationEnd oAnimationEnd', function(){
	Mediator.publish('box2end');
});

$('#box3').bind('animationend webkitAnimationEnd MSAnimationEnd oAnimationEnd', function(){
	Mediator.publish('box3end');
});
